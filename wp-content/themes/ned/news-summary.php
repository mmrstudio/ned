
                <div class="news-article-summary">
					 <div class="thumbnail">
                        <a href="<?php the_permalink(); ?>">

                            <div class="date">
                                <span ><?php echo get_the_date('j', get_the_ID() ); ?></span>
                                <?php echo get_the_date('M', get_the_ID() ); ?>
                                <?php echo get_the_date('Y', get_the_ID() ); ?>
                            </div>

                            <?php the_post_thumbnail('news-banner-small'); ?>

                        </a>
                    </div>
                    

                    <h5 class="title"><?php the_title(); ?></h5>

                    <div class="excerpt">
                        <p><?php the_excerpt(); ?></p>
                    </div>

                </div>
