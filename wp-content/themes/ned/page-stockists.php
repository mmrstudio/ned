<?php 

    /* Template Name: Page - Stockists */

   if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

         // load header
         get_header();
        
        // get state
       echo $state = (isset($_GET['state']) ? $_GET['state'] : false);


		
		
 		
	   
        
        // get state info
        switch($state) :
            case 'victoria' : $state_info = array('name' => 'Victoria', 'lat' => -37.479332, 'lon' => 144.983718, 'zoom' => 7); break;
            case 'new-south-wales' : $state_info = array('name' => 'New South Wales', 'lat' => -32.472695, 'lon' => 146.931152, 'zoom' => 6); break;
            case 'queensland' : $state_info = array('name' => 'Queensland', 'lat' => -21.698265, 'lon' => 146.997070, 'zoom' => 5); break;
            case 'south-australia' : $state_info = array('name' => 'South Australia', 'lat' => -31.391158, 'lon' => 135.351563, 'zoom' => 6); break;
            default : $state_info = array('name' => false, 'lat' => -24.994167, 'lon' => 134.866944, 'zoom' => 4); break;
        endswitch;

?>

    <div class="container-fluid page-banner">

        <div class="container" style="border-bottom: none;">
            <h2>Where to Buy<?php echo ($state_info['name']) ? ' - ' . $state_info['name'] : ''; ?></h2>
        </div>

    </div>

    <div class="container-fluid">

		<div class="container">

				<div class="row">

					<article class="col-xs-12 col-md-8">

						<script type="text/javascript">

							jQuery(document).ready(function($) {

								var stockists = <?php echo get_stockists_json($state); ?>;
								var mapPin = '<?php echo get_template_directory_uri() . '/images/map-pin.png'; ?>';
								var mapPinColor = '<?php echo get_template_directory_uri() . '/images/map-pin-color.png'; ?>';
								var mapCenter = new google.maps.LatLng(<?php echo $state_info['lat']; ?>, <?php echo $state_info['lon']; ?>);
								var mapZoom = <?php echo $state_info['zoom']; ?>;
								
								//console.log(mapZoom + "222222222222222222222");

								initializeGoogleMap(stockists, mapPin, mapPinColor, mapCenter, mapZoom);

								countStockists();

							});

						</script>


						<div class="google-search-box-row">



							<div class="google-search-box">
								<div class="row">
									<div class="search-box col-sm-6">
										<input type="text" class="input" id="mapAutocomplete" placeholder="Postcode">

										 <!--<select>
										  <option value="all">All NED Products</option>
										  <option value="4.8">4.8% Whisky &amp; Cola 375ml</option>
										  <option value="6.0">6.0% Whisky &amp; Cola 375ml</option>
										  <option value="9.0">9.0% Whisky &amp; Cola 250ml</option>
										  <option value="4.8dry">4.8% Whisky &amp; Dry 375ml</option>
										</select> -->
									</div>
									<div class="col-sm-6">
										<button class="search-btn btn btn-orange" id="searchMap">Search</button>
									</div>
								</div>			
							</div>


						</div>

						<div class="google-map-row">
							<div class="google-map" id="map-canvas"></div>
							<?php if($state) : ?>
							<div class="selected-state">Currently viewing stockists in <strong><?php echo $state_info['name']; ?></strong>. <a href="<?php echo site_url('/where-to-buy/'); ?>">Show All</a></div>
							<?php endif; ?>
						</div>




					</article>

					<aside class="col-xs-12   col-md-4 sidebar stocklist" id="sidebar">
						 <p class="stocklist__note">*Please contact store to confirm NED product/s are currently in stock.</p>
						 <div id="up" class="icon-arrow-up">up</div>
						 <div class="stockists-wrap" id="parent">

							<!--<div class="state-dropdown">
								<button class="open-dropdown btn btn-black btn-lg icon icon-caret"><?php echo ($state) ? $state_info['name'] : 'Select State...' ?></button>
								<ul class="dropdown">
									<?php if(!$state) : ?>
									<li><a href="<?php echo site_url('/where-to-buy/'); ?>">Show All</a></li>
										<h2>ENTER YOUR POSTCODETO FIND NED</h2>
									<?php endif; ?>
									<?php foreach(get_stockist_states() as $state_slug => $state_name) : ?>
									<li><a href="<?php echo site_url('/where-to-buy/?state=' . $state_slug); ?>"><?php echo $state_name; ?></a></li>
									<?php endforeach; ?>
								</ul>
							</div>-->
							
							

						<?php 
							 
							 foreach(get_stockist_states($state) as $state_slug => $state_name) : ?>

						<div class="state">

							<!--<h3 class="state-header"><?php echo $state_name; ?></h3>-->

							<?php  
							foreach(get_stockists($state_slug) as $stockist) : ?>
							<div class="row stockist-summary  child" data-stockist-id="<?php echo $stockist['id']; ?>">

							   <!-- <div class="col-xs-12 col-sm-3">
									<h4>Retailer:</h4>
									<p><strong><?php echo $stockist['name']; ?></strong></p>
								</div>-->

								<div class="col-xs-12 col-sm-12 stockist-col">
									<p class="shopname"><strong><?php echo $stockist['name']; ?></strong></p>
									<p>
										<?php echo $stockist['street_address']; ?><br>
										<?php echo $stockist['suburb']; ?> , 
										<?php echo $stockist['state_abbr']; ?> <?php echo $stockist['postcode']; ?><br>
										<?php if($stockist['phone']) : ?><span class="phone"><?php echo $stockist['phone']; ?></span><br><?php endif; ?>
										<?php /*?><?php if($stockist['website']) : ?><span class="website"><a href="<?php echo $stockist['website']; ?>" target="_blank"><?php echo $stockist['website']; ?></a></span><br><?php endif; ?>
										<?php if($stockist['email']) : ?><span class="email"><a href="mailto:<?php echo $stockist['email']; ?>"><?php echo $stockist['email']; ?></a></span><br><?php endif; ?><?php */?>
									</p>
								</div>

								<!--<div class="col-xs-12 col-sm-6 stockist-col">
									<?php if($stockist['opening_hours']) : ?>
									<h4>Opening hours:</h4>
									<p><?php echo $stockist['opening_hours']; ?></p>
									<?php endif; ?>
								</div>

								<div class="col-xs-12 col-sm-3 map-link stockist-col">
									<a href="<?php echo $stockist['map_link']; ?>" target="_blank" class="btn btn-orange btn-arrow btn-arrow-right">View Map</a>
								</div>-->


							</div>
							<?php endforeach; ?>

						</div>

						<?php endforeach; 
						 ?>
						</div>
						 <div id="down" class="icon-arrow-down">down</div>

						<div class="no-stockists" id="noStockists">
							<p>There are no stockists to display with your current search parametres.</p>
						</div>
						<div class="search_message">
									
							ENTER YOUR POSTCODE<br> TO FIND NED
						</div>


					</aside>

				</div>

			</div>

		 <?php

			$get_products = new WP_Query(array(
				'post_status' => 'publish',
				'post_type' => 'product',
				'posts_per_page' => 4,

			));

		?>
    	<div class="container buyonline">
    		<?php /* ?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 buyonline__heading">
					<h2>CAN’T FIND NED IN YOUR LOCAL STORE?</h2>
					<p>Free delivery Australia-wide</p>
				</div>
				<div class="col-xs-12 col-md-8 col-md-push-2 col-lg-6 col-lg-push-3">
				<?php
					
					  if ( $get_products->have_posts() ) :
					  while ( $get_products->have_posts() ) : $get_products->the_post();  ?>
						<div class="col-xs-12 col-sm-3 col-md-3">
							 <div class="buyonline__details">

								<div class="buyonline__details__thumbnail">
									  <?php $thumbnail = get_field('thumbnail'); ?>
									  <img src="<?php echo $thumbnail['url']; ?>">
								</div>
								<div class="buyonline__details__url">
									<?php if (get_field('buy_url')): ?>
									<a href="<?php the_field('buy_url'); ?>" target="_blank" class="btn btn-orange-white product_details <?php the_title(); ?>" >Buy online</a>
									<?php endif; ?>
								 </div>
							</div>
						</div>
						 <?php 	endwhile; 
								endif; ?>
				</div>		
		  </div>
		  <?php */ ?>
		</div>	          
    
   
	</div>	

<?php

        get_footer();

    endwhile; // end the loop
