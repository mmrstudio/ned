<?php

    // Check Age
/*
    $age_approved = age_approved();
    if($age_approved === false) :
        wp_redirect('/age-gate');
    endif;
*/

   
?><!DOCTYPE HTML>

<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title><?php echo get_page_title(); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<?php wp_head(); ?>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,600,700,800' rel='stylesheet' type='text/css'>

        <?php echo google_analytics('UA-58883042-1', 'auto'); ?>

        <?php if(is_page('buy')) : ?>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyC7Y_yRqKoeOM1fHUu23l6euzvnEJPVJCE"></script>
        <?php endif; ?>



    <style>

        #map-canvas img { max-width: none; }
        .page-wrap.home-header-wrap, .age-gate.header-wrap {
            background-image: url('<?php echo get_field('banner','option');?>');
            background-size: cover;
        }
        
    </style>

    </head>

    <body>
	<?php $option =  get_field('option','option'); 
		
		if($option == "Image"  ) { 
?>
   
       
        <div class="container-fluid  age-gate header-wrap  " id="ageGate">
    
            <div class="container-fluid hero-banner">
    
                <div class="container header">
                	<div class="row">

                        <div class="col-xs-12 col-sm-12">
    
                            <div class="ned-logo">
                                <a href="/" class="icon icon-nedlogo white">Drink NED</a>
                            </div>
                        </div>

                   

               	    </div>
    
                    <div class="row">
    
                        <div class="col-xs-12 col-sm-12 age-gate__text">
							<div><p>Are you old enough to enjoy a NED?</p></div>
    
                     
							
							<a href="#" id="ageOkay" class="yes"><h2>YEAH</h2></a>
							<a href="#" id="ageBad"><h2>NAH</h2></a>		
                                   

                            
                              
    
                        </div>
    					
                                   
    
                    </div>
        
                </div>
        
            </div>
    		<div class="banner_shape">
               
            </div>
            
    
        </div>
 <?php } else {  ?>

  
 <div class="container-fluid  age-gate header-wrap" id="ageGate">
    
    <div class="video-back">
           <div class="overlay"></div>
                <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                    <source src="<?php echo get_field('banner_video','option');?>" type="video/mp4">
                </video>
                  <div class="container header">
                    <div class="row">

                        <div class="col-xs-12 col-sm-12">
    
                            <div class="ned-logo">
                                <a href="/" class="icon icon-nedlogo white">Drink NED</a>
                            </div>
                        </div>

                   

                    </div>
    
                    <div class="row">
    
                        <div class="col-xs-12 col-sm-12 age-gate__text">
                            <div><p>Are you old enough to enjoy a NED?</p></div>
    
                     
                            
                            <a href="#" id="ageOkay" class="yes"><h2>YEAH</h2></a>
                            <a href="#" id="ageBad"><h2>NAH</h2></a>        
                                   

                            
                              
    
                        </div>
                        
                                   
    
                    </div>
        
                </div>
                <div class="banner_shape">
               
            </div>
    </div>
          
</div>
    <?php } ?>
        
        <button class="responsive-nav-toggle icon-mobile_menu <?php if ( is_page_template( 'page-home.php' )) echo "home-mobile-menu"; ?>" id="responsiveNavToggle"></button>
		
        <div class="responsive-nav-wrap">
            <nav class="responsive-nav">
                <?php output_main_nav(2); ?>
				<?php get_template_part('content', 'social-icons'); ?>
            </nav>
            <div class="responsive-nav-dismiss" id="responsiveNavDissmiss"></div>
        </div>
<?php 
if($option == "Image"  ) { 
?>

        <div class="container-fluid page-wrap header-wrap <?php if ( is_page_template( 'page-home.php' )) echo "home-header-wrap"; ?>">

            <header class="container header">

                <div class="row">

                    <div class="col-xs-12 col-sm-12">

                        <div class="ned-logo">
                            <a href="/" class="icon icon-nedlogo">Drink NED</a>
                        </div>
                        
                        
                        <nav class="main-nav hidden-xs">
                            <?php output_main_nav(1); ?>
                        </nav>

                    </div>

                   

                </div>

                

            </header>
             <div class="header-social hidden-xs">

				<?php get_template_part('content', 'social-icons'); ?>

            </div>
            <?php if ( is_page_template( 'page-home.php' )){ ?>
            <div class="banner_shape">
            	<a href="#" class="scroll_button"></a>
            </div>
            <?php } ?>
       </div> 
   <?php } else { // Video?>
 <div class="container-fluid page-wrap header-wrap <?php if ( is_page_template( 'page-home.php' )) echo "home-header-wrap-video"; ?>">
         
    <div class="video-back">
         <?php  if ( is_page_template( 'page-home.php' )) { ?>
          <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="<?php echo get_field('banner_video','option');?>" type="video/mp4">
          </video>
      <?php } ?>
       </div> 

              <header class="container header">

                <div class="row">

                    <div class="col-xs-12 col-sm-12">

                        <div class="ned-logo">
                            <a href="/" class="icon icon-nedlogo">Drink NED</a>
                        </div>
                        
                        
                        <nav class="main-nav hidden-xs">
                            <?php output_main_nav(1); ?>
                        </nav>

                    </div>

                   

                </div>

                

            </header>
              <div class="header-social hidden-xs">

                <?php get_template_part('content', 'social-icons'); ?>

            </div>
            <?php if ( is_page_template( 'page-home.php' )){ ?>
            <div class="banner_shape">
                <a href="#" class="scroll_button"></a>
            </div>
            <?php } ?>
   
  
</div>
   <?php } ?>
       <div class="container-fluid page-wrap">    

