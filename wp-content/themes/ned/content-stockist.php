
                <div class="row stockist-summary">

                    <div class="col-xs-12 col-sm-3">
                        <h4>Retailer:</h4>
                        <p><strong><?php the_title(); ?></strong></p>
                    </div>

                    <div class="col-xs-12 col-sm-3 stockist-col">
                        <h4>Contact details:</h4>
                        <p>
                            <?php echo get_field('street_address'); ?><br>
                            <?php echo get_field('suburb'); ?>, <?php echo get_field('state'); ?> <?php echo get_field('postcode'); ?><br>
                            <span class="phone"><?php echo get_field('phone'); ?></span>
                        </p>
                    </div>

                    <div class="col-xs-12 col-sm-4 stockist-col">
                        <h4>Opening hours:</h4>
                        <?php echo get_field('opening_hours'); ?>
                    </div>

                    <div class="col-xs-12 col-sm-2 map-link stockist-col">
                        <a href="<?php echo get_field('map_link'); ?>" target="_blank" class="btn btn-orange btn-arrow btn-arrow-right">View Map</a>
                    </div>


                </div>
