<?php

    /* Template Name: Page - Promotions */

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

         // load header
         get_header();

?>

    <div class="container-fluid page-banner">

        <div class="container">
            <h1>Promotions</h1>
        </div>

    </div>

            <div class="container-fluid">

                <div class="container">

                    <div class="row">

                        <article class="col-xs-12 col-md-8 content-col">

                            <div class="row">
                    
                                <?php
                    
                                $get_latest_posts = new WP_Query(array(
                                    'post_status' => 'publish',
                                    'post_type' => 'promotion',
                                    'posts_per_page' => 3
                                ));
                    
                                ?>
                    
                                <?php if ( $get_latest_posts->have_posts() ) : while ( $get_latest_posts->have_posts() ) : $get_latest_posts->the_post(); ?>
                                <div class="col-xs-12 news-article-summary-wrap horizontal">
                                    <?php get_template_part('promotion', 'summary-horizontal'); ?>
                                </div>
                                <?php endwhile; endif; wp_reset_query(); ?>
                    
                            </div>

                        </article>

                        <aside class="col-xs-4 sidebar hidden-xs hidden-sm" id="sidebar">
                            <?php get_sidebar(); ?>
                        </aside>

                    </div>

                </div>

            </div>

<?php

        get_footer();

    endwhile; // end the loop
