			
            
            
            
            
            <div class="container-fluid page-wrap newsletter">

				<div class="container">
					<div class="row">
	
	                     

                        <div class="col-xs-12 col-sm-12  col-md-8 signup col-md-push-2">
                        
                        	 <div class="row">
                             	<div class="col-xs-12 col-sm-4 ">
							 		<h2>JOIN THE GANG</h2> 
								</div>
								<div class="col-xs-12 col-sm-8">
									<?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]'); ?>
                                </div>
                            </div>
                            <div class="row"> 
                            	<div class="col-xs-12 col-sm-12 bottomtext">   
                             		<p>Sign up and get access to exciting offers and promotions</p>
                                </div>
                            </div>        
	                    </div>
	
					</div>
				</div>

			</div>

            <footer class="container-fluid footer page-wrap">

                <div class="container">
                	<div class="row">
                         <div class="firstrow col-xs-12 col-md-8 col-md-push-2 col-lg-8 col-lg-push-2" >
                            <div class="ned-logo">
                                <a href="/" class="icon icon-nedlogo">Drink NED</a>
                            </div>
                                
                            <div class="footer-menu main-nav">
                                <?php output_main_nav(2); ?>
                               
                            </div>
                            <div class="social">
                                 <?php get_template_part('content', 'social-icons'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-xs-12 copyright">
							<p>&copy; <?php echo date('Y'); ?> Top Shelf International &nbsp;<span class="line">|</span>&nbsp; <a href="<?php echo site_url('/terms-conditions/'); ?>">Terms of Use</a>&nbsp;<span class="line">|</span>&nbsp;<a href="http://mmr.com.au" target="_blank">Site by MMR</a></p>
                           
                        </div>

                    </div>
                    
                    

                </div>

            </footer>

        </div>

<?php wp_footer(); ?>

    </body>

</html>
