
                <div class="news-article-summary horizontal">

                    <h3 class="title"><?php the_title(); ?></h3>

                    <div class="col-xs-12 col-sm-6">

                        <div class="thumbnail">
                            <a href="<?php the_permalink(); ?>">
    
                                <div class="date">
                                    <span class="day"><?php echo get_the_date('j', get_the_ID() ); ?></span><br>
                                    <?php echo get_the_date('M', get_the_ID() ); ?><br>
                                    <?php echo get_the_date('Y', get_the_ID() ); ?>
                                </div>
    
                                <?php the_post_thumbnail('news-banner-small'); ?>
    
                            </a>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6">

                        <div class="excerpt">
                            <p><?php the_excerpt(); ?></p>
                        </div>
    
                        <div class="read-more">
                            <a href="<?php the_permalink(); ?>" class="btn btn-orange btn-arrow btn-arrow-right">Read More</a>
                        </div>

                    </div>

                </div>
