<?php

    /* Template Name: Page - Single Column */

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

         // load header
         get_header();

?>

    <div class="container-fluid page-banner">

        <div class="container">
            <h2><?php the_title(); ?></h2>
        </div>

    </div>

    <div class="container-fluid">

        <div class="container">

            <div class="row">

                <article class="col-xs-12 content-col page-content no-padding">
                    <?php the_content(); ?>
                </article>

            </div>

        </div>

    </div>

<?php

        get_footer();

    endwhile; // end the loop
