<?php

    /* Template Name: Page - Home */

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

         // load header
         get_header();

?>

    
    <?php

	    $get_products = new WP_Query(array(
	        'post_status' => 'publish',
	        'post_type' => 'product',
	        'posts_per_page' => 4,
          'tax_query' => array(
          array(
              'taxonomy' => 'product_cat',
              'field'    => 'slug',
              'terms'    => 'home',
              ),
          ),

	    ));

    ?>
	<div class="container home-products">
        <div class="row">
    		<div class="col-xs-12 col-sm-12 home-products__heading">
            	<h1><?php echo get_field('big_title');?></h1>
                <p><?php echo get_field('small_text');?></p>
            </div>
            <?php
			      $i=0;
				  $thumb_list='';
				  if ( $get_products->have_posts() ) :
				  while ( $get_products->have_posts() ) : $get_products->the_post(); $i++; ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
                 <div class="home-products__details col-xs-12">
    
                    <div class="home-products__details__thumbnail col-xs-5 col-sm-12">
                          <?php $thumbnail = get_field('thumbnail'); ?>
                          <img src="<?php echo $thumbnail['url']; ?>">
                    </div>
    				<div class="home-products__details__discription col-xs-7 col-sm-12">
						<h3 class="title"><?php the_title(); ?></h3>


						<h4 class="<?php the_title(); ?>"><?php the_field('heading_2'); ?></h4>

						<?php the_field('home_page_discription'); ?>
						
					<!-- 	<a href="/buy/" class="btn btn-orange wheretobuy <?php //the_title(); ?>">WHERE TO BUY</a> -->

						<a href="<?php the_permalink(); ?>" class="btn btn-orange-white product_details slide<?php echo $i; ?>  <?php the_title(); ?>" >DETAILS</a>


						<?php $thumb_list.='<li><a class="'.get_the_title().' slide'.$i.'" href="#"><img class="thumb'.$i.'" src="'.$thumbnail['url'].'"><div class="thumbbg"></div></a></li>'; ?>
					 </div>
                </div>
            </div>
             <?php 	endwhile; 
			 		endif; ?>
      </div>
           
      <?php if ( $get_products->have_posts() ) : while ( $get_products->have_posts() ) : $get_products->the_post(); ?>
      
      <div class="row"> 
            <div class="col-xs-12 col-sm-12">
                 <div class="home-products__single">
                 	<div class="row">
						  <div class="col-xs-12 col-sm-12">
				  	 	 	  <h2>PRODUCT DETAILS</h2>
					  	 	 <ul>
						  	 <?php echo $thumb_list; ?>
							  </ul>
							  
							  <span class="close"></span>
							 
						  </div>
					</div>
    				<div class="row">
                        <div class="home-products__single__image col-sm-12 col-md-6">
                              <?php $image = get_field('product_image'); ?>
                              <img src="<?php echo $image['url']; ?>">
                        </div>
                        <div class="home-products__single__discription col-sm-12 col-md-6">
                            <h1 class="title"><?php the_title(); ?></h1>
        
                       <h4  ><?php the_field('heading_2'); ?></h4>
                        <!--      <h4 style="color:<?php the_field('color_code'); ?>"><?php the_field('heading_2'); ?></h4> -->
                        
                            <?php the_field('heading_3'); ?>
                            
							<div class="discription"><?php the_field('discription'); ?></div>
                        	
                        	<!-- <a href="/buy/" class="btn btn-orange wheretobuy <?php //the_title(); ?>">WHERE TO BUY</a> -->
                        	
                         	<?php if (get_field('buy_url')): ?>
                            <a href="<?php the_field('buy_url'); ?>" target="_blank" class="btn btn-orange-white product_details <?php the_title(); ?>" >Buy online</a>
                            <?php endif; ?>
                        </div>
                     </div>   
                </div>
            </div>
       </div>
      
       <?php endwhile; endif; wp_reset_query(); ?>
    
        
   </div>
   
    <div class="container home-legend">

        <div class="row">
			<div class="col-xs-12">
           		<h2 class="mobile_title"><?php the_field('legend_title'); ?></h2>
			</div>		
            <div class="col-xs-12 col-sm-3 thumbnail col-sm-push-1">
            	<?php  $thumbnail = get_field('legend_thumbnail'); ?>
                 <img src="<?php echo $thumbnail['url']; ?>">
            </div>
            <div class="col-xs-12 col-sm-6 discription col-sm-push-1">
            	<h2 class="title"><?php the_field('legend_title'); ?></h2>
                <?php the_field('legend_text'); ?>
            </div>
        </div>
    </div>  
</div> 
<?php /*?>
<div class="container-fluid page-wrap promotions" >              

    <div class="container home-promotions">

        <div class="row">

            <div class="col-xs-12 col-sm-6 home-promotions__left">


                    <h2>News</h2>
            
                    
					<div class="row">

						
						<div class="col-xs-12 col-sm-12">
							<div class="discription">
								<h5><?php the_field('news_title'); ?></h5>
								<?php the_field('news_discription'); ?>
							</div>
						</div>
						

					</div>

					<div class="row">

						
						<div class="col-xs-12">

							<div class="read-more">
								<a href="/news" class="btn btn-orange btn-arrow">Read News</a>
							</div>

						</div>
						

					</div>
            
                
            </div>

            <div class="col-xs-12 col-sm-6 home-promotions__right">
                <h2>WHERE TO BUY</h2>
                <div class="row">
                	<div class="col-xs-12 col-sm-12">
                        <div class="discription">
                            <h5>Instore or Online</h5>
                            <p>Check out where you can get your hands on NED.<br>
                            *Call ahead to confirm stock is currently instore
                            </p>
                        </div>
                    </div>
                </div>        
                <div class="row">
                	<div class="col-xs-12">
                        <div class="read-more">
                            <a href="/buy/" class="btn btn-orange btn-arrow">Click Here</a>
                        </div>
                    </div>
                </div>        
            </div>

        </div>

    </div>
</div>
<?php */ ?>
<!-- Socials -->
<?php 
//require 'includes/socials.php';
//$instagram = instagram_get_feed('1527865258.1677ed0.43ea98b0c2a54d1097afa39ba8773afa');


// Facebook
//$facebook_token = '294787000878623|Z_EYJC5fTAegG4cz6IxQ1tiyzn0';
//$facebook_page_id = '267222336993801';
//$data = facebook_get_feed($facebook_token, $facebook_page_id);
//$facebook_1 = $data['data'][0];

//$facebook_2 = $data['data'][1];

//var_dump($instagram);

//foreach ($instagram['data'] as $key => $post) { $i++; 


//$twitter_posts = get_social_posts('twitter', $pg);
$instagram_posts = get_social_posts('instagram', 11);
//$facebook_posts = get_social_posts('facebook', 3);
sync_social_feed();
$instagram_posts = ($instagram_posts['posts']);
//var_dump($facebook_posts );
?>
			
<!-- <div class="container-fluid social page-wrap"> -->
<!-- <h2>RIDE WITH NED</h2>  -->
<?php //echo wdi_feed(array('id'=>'1')); ?>





<!-- </div> -->

<!-- Socials END -->
            



   

<?php

        get_footer();

    endwhile; // end the loop
