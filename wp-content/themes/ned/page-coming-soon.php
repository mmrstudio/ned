<?php

    /* Template Name: Coming Soon */

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

         // load header
         get_header('coming-soon');

?>

    <div class="container-fluid hero-banner coming-soon">

        <div class="container">

            <div class="coming-soon-message">
                <p>WEBSITE LAUNCHING JAN 2015</p>
                <p>JOIN THE GANG <span class="handle"><span class="icon icon-at"></span>DRINKNED</span> <br>FOR NEWS ON SPECIAL <br>PROMOTIONS, EVENTS AND <br>WHERE TO BUY NED!</p>

                <ul class="social-icons">
                    <li><a href="https://www.facebook.com/drinkned" target="_blank" class="linked-in icon icon-facebook">Facebook</a></li>
                    <li><a href="https://twitter.com/drinkned" target="_blank" class="twitter icon icon-twitter">Twitter</a></li>
                    <li><a href="https://www.youtube.com/channel/UCV5fLubdxRCOFJZ8LGZdyQQ" target="_blank" class="twitter icon icon-youtube">YouTube</a></li>
                    <li><a href="http://instagram.com/drinkned/" target="_blank" class="twitter icon icon-instagram">Instagram</a></li>
                </ul>

            </div>

            <div class="ned-can"></div>

            <div class="over-18"></div>

        </div>

    </div>

<?php

        get_footer();

    endwhile; // end the loop
