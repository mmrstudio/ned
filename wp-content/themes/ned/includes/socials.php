<?php


// Facebook
$access_token = '294787000878623|Z_EYJC5fTAegG4cz6IxQ1tiyzn0';
function facebook_get_feed($token, $id) {
  $result = facebook_check_cache($token, $id);
  if(!$result) {
    $result = facebook_load_feed($token, $id);
    facebook_save_feed($result);
  }
  $result = json_decode($result, true);
  return $result;
}

function facebook_get_attachment($id, $access_token) {
  $url = "https://graph.facebook.com/v2.7/" . $id . "/attachments?access_token=" . $access_token;
  return json_decode(file_get_contents($url));
}

function facebook_load_feed($access_token, $id) {
  if(!$id) {
    $id = "276470134564";
  }
  if(!$access_token) {
    global $access_token;
  }
  $url = "https://graph.facebook.com/v2.7/" . $id . "/feed?access_token=" . $access_token;
  $data  = file_get_contents($url);
  return $data;
}

function facebook_save_feed($data) {
  $dir = get_stylesheet_directory() . '/cache';
  $filename = $dir . '/facebook.txt';
  $data = facebook_set_timestamp($data);
  file_put_contents($filename, $data);
}

function facebook_set_timestamp($data) {
  $data = json_decode($data, true);
  $data['timestamp'] = time();
  $data = json_encode($data);
  return $data;
}

function facebook_check_cache($token, $id) {
  $dir = get_stylesheet_directory() . '/cache';
  $filename = $dir . '/facebook.txt';
  if (file_exists($filename)) {
    $data = file_get_contents($filename);
    $temp = json_decode($data, true);
    $timestamp = $temp['timestamp'];
    $current_timestamp = time();
    /*echo $timestamp;
    echo '<br>';
    echo $current_timestamp;*/
    if(($current_timestamp - $timestamp) > 0) { //3600 seconds = 1 hour
      // Invaliadate cache
      $data = facebook_load_feed($token, $id);
      $data = facebook_set_timestamp($data);
      file_put_contents($filename, $data);
    }
    return $data;
  } else {
    return false;
  }
}



// Instagram
function instagram_get_feed($token) {
  $result = instagram_check_cache($token);
  if(!$result) {
    $result = instagram_load_feed($token);
    instagram_save_feed($result);
  }
  $result = json_decode($result, true);
  return $result;
}

function instagram_load_feed($access_token) {

//$url = "https://api.instagram.com/v1/users/" . $id . "/media/recent?access_token=" . $access_token;
  $url = "https://api.instagram.com/v1/users/self/media/recent/?count=7&access_token=" . $access_token;
  $data  = file_get_contents($url);
  //var_dump($data);
  return $data;
 
}

function instagram_save_feed($data) {
  $dir = get_stylesheet_directory() . '/cache';
  $filename = $dir . '/instagram.txt';
  $data = instagram_set_timestamp($data);
  file_put_contents($filename, $data);
}

function instagram_set_timestamp($data) {
  $data = json_decode($data, true);
  $data['timestamp'] = time();
  $data = json_encode($data);
  return $data;
}

function instagram_check_cache($token) {
  $dir = get_stylesheet_directory() . '/cache';
  $filename = $dir . '/instagram.txt';
  if (file_exists($filename)) {
    $data = file_get_contents($filename);
    $temp = json_decode($data, true);
    $timestamp = $temp['timestamp'];
    $current_timestamp = time();

    if(($current_timestamp - $timestamp) > 3600) {
// Invaliadate cache
      $data = instagram_load_feed($token);
      $data = instagram_set_timestamp($data);
      file_put_contents($filename, $data);
    }
    return $data;
  } else {
    return false;
  }
}