<?php

    //add_image_size('download-image', 555 , 0 , false );
    //add_image_size('download-thumb', 600 , 600 , true );

    // define cpt
    $cpt_labels = array(
        'post_type_name' => 'social',
        'singular' => 'Social Feed',
        'plural' => 'Social Feed',
        'slug' => 'social'
    );

    // define cpt options
    $cpt_options = array(
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => 'social',
            'pages' => false,
            'with_front' => false,
        ),
        'has_archive' => false,
    );

    // create cpt
    $social_cpt = new CPT($cpt_labels, $cpt_options);

    // set dashicon
    $social_cpt->menu_icon('dashicons-networking');

    $social_cpt->columns(array(
        'cb' => '<input type="checkbox" />',
        'thumbnail' => 'Thumbnail',
        'title' => __('Title'),
        'platform' => 'Platform',
        'link' => 'Link',
        'date' => __('Date')
    ));

    $social_cpt->populate_column('thumbnail', function($column, $post) {
        if(get_field('image')) :
            echo '<img src="'. get_field('image') . '" style="display:block; width:50px;">';
        else :
            echo '<img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=X&w=100&h=100&txttrack=0" style="display:block; width:50px;">';
        endif;
    });

    $social_cpt->populate_column('platform', function($column, $post) {
        echo get_field('platform');
    });

    $social_cpt->populate_column('link', function($column, $post) {
        echo '<a href="'. get_field('link') . '" target="_blank">' . get_field('link') . '</a>';
    });

    function get_social_posts($platform, $ppp=false, $page=false) {

        $ppp = !$ppp ? 5 : $ppp;

        $social_posts = [
            'posts' => [],
            'max_num_pages' => 1,
        ];

        $query = [
            'post_type' => 'social',
            'post_status' => 'publish',
            'meta_query' => [
                'relation' => 'AND',
                [ 'key' => 'platform', 'value' => $platform, 'compare' => '='],
                [ 'key' => 'hide_post', 'value' => '0', 'compare' => '='],
            ],
            'posts_per_page' => $ppp,
            'orderby' => 'date',
	        'order' => 'DESC',
        ];

        if($page) $query['paged'] = $page;

        $get_social_posts = new WP_Query($query);

        if($get_social_posts->have_posts()) :

            while($get_social_posts->have_posts()) : $get_social_posts->the_post();

                $social_post = [
                    'id' => get_the_ID(),
                    'date' => get_the_date(),
                    'date_hr' => time2str(get_the_date('U')),
                ];

                $social_posts['posts'][] = array_merge($social_post, get_fields());

            endwhile;

            $social_posts['max_num_pages'] = $get_social_posts->max_num_pages;

            wp_reset_postdata();

        endif;

        return $social_posts;

    }

    function sync_social_feed($num_posts=20) {

       $delete_transient = false;
       $delete_transient = true;

       if(false === get_transient('social_posts')) :

           $instagram_posts = get_instagram_feed($num_posts, $delete_transient);
           $twitter_posts = get_latest_tweets($num_posts, $delete_transient);
           $facebook_posts = get_latest_facebook_posts($num_posts, $delete_transient);

           $social_posts = array_merge($instagram_posts, $twitter_posts, $facebook_posts);

           // save transient with expiry
           set_transient('social_posts', $social_posts, 2 * HOUR_IN_SECONDS);

           foreach($social_posts as $social_post) :

               $post_title = $social_post->platform . '-' . $social_post->id;

               // create instagram post
               $check_existing = get_page_by_title($post_title, 'OBJECT', 'social');

               if(! $check_existing) :

                   $new_post = wp_insert_post([
                       'post_type' => 'social',
                       'post_title' => $post_title,
                       'post_status' => 'publish',
                       'post_date' => date('Y-m-d H:i:s', strtotime($social_post->timestamp)),
                   ]);

                   update_field('field_581ac442a3700', 0, $new_post); // hide post
                   update_field('field_581ac036ccc4b', $social_post->platform, $new_post);
                   update_field('field_581ac04fccc4e', $social_post->type, $new_post);
                   update_field('field_581ac03eccc4c', $social_post->id, $new_post);
                   update_field('field_581ac049ccc4d', $social_post->link, $new_post);
                   update_field('field_581ac057ccc4f', $social_post->text, $new_post);
                   update_field('field_581ac070ccc50', $social_post->image, $new_post);
                   update_field('field_581bce7d81ac6', $social_post->image_aspect, $new_post);

                   if(isset($social_post->likes)) update_field('field_581ac080ccc51', $social_post->likes, $new_post);
                   if(isset($social_post->comments)) update_field('field_581ac086ccc52', $social_post->comments, $new_post);
                   if(isset($social_post->retweets)) update_field('field_581ac094ccc53', $social_post->retweets, $new_post);
                   if(isset($social_post->favorites)) update_field('field_581ac0c56be25', $social_post->favorites, $new_post);

               endif;

           endforeach;

       endif;

       //print_r($social_posts); exit;

       return $social_posts;

   }