<?php

    // add images sizes
    add_image_size('ned-tv-thumb', 370, 210, true);
    add_image_size('ned-tv-thumb-2x', 740, 420, true);
    add_image_size('ned-tv-poster', 720, 405, true);
    add_image_size('ned-tv-poster-2x', 1440, 810, true);

    // define cpt
    $cpt = array(
        'post_type_name' => 'ned-tv',
        'singular' => 'NED TV',
        'plural' => 'NED TV',
        'slug' => 'ned-tv'
    );

    // define cpt options
    $options = array(
        'supports' => array('title', 'editor', 'page-attributes'),
        'rewrite' => array(
            'slug' => 'ned-tv',
            'pages' => false
        ),
    );

    // create cpt
    $ned_tv_cpt = new CPT($cpt, $options);

    // set dashicon
    $ned_tv_cpt->menu_icon('dashicons-format-video');

    // function to retrieve videos
    function get_ned_tv($ppp=1, $order_by='post_date', $order='DESC') {

        $videos = array();

        // define query params
        $videos_query = array(
            'post_status' => 'publish',
            'post_type' => 'ned-tv',
            'orderby' => $order_by,
            'order' => $order,
            'posts_per_page' => $ppp
        );

        // query db
        $get_videos = new WP_Query($videos_query);

        // get result
        $s = 1;
        if ( $get_videos->have_posts() ) : while ( $get_videos->have_posts() ) : $get_videos->the_post();

    		$video = array(
    		    'id' => get_the_ID(),
    		    'permalink' => get_the_permalink(),
    		    'title' => get_the_title(),
    		    'description' => apply_filters('the_content', get_the_content()),
    		    'image' => get_field('video_thumbnail'),
    		    'youtube_url' => get_field('youtube_url')
    		);

            $videos[] = $video;

            $s++;

        endwhile; endif;
        
        wp_reset_query();

        return ($ppp > 1) ? $videos : $videos[0];

    }
