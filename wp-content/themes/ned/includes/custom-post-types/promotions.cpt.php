<?php

    // define cpt
    $cpt = array(
        'post_type_name' => 'promotion',
        'singular' => 'Promotion',
        'plural' => 'Promotions',
        'slug' => 'promotions'
    );

    // define cpt options
    $options = array(
        'supports' => array('title', 'editor', 'page-attributes'),
        'rewrite' => array(
            'slug' => 'promotion',
            'pages' => false
        ),
    );

    // create cpt
    $promotions_cpt = new CPT($cpt, $options);

    // set dashicon
    $promotions_cpt->menu_icon('dashicons-star-filled');

    // function to retrieve videos
    function get_promotions($ppp=1, $order_by='post_date', $order='DESC') {

        $promotions = array();

        // define query params
        $promotions_query = array(
            'post_status' => 'publish',
            'post_type' => 'promotion',
            'orderby' => $order_by,
            'order' => $order,
            'posts_per_page' => $ppp
        );

        // query db
        $get_promotions = new WP_Query($promotions_query);

        // get result
        $s = 1;
        if ( $get_promotions->have_posts() ) : while ( $get_promotions->have_posts() ) : $get_promotions->the_post();

    		$promotion = array(
    		    'id' => get_the_ID(),
    		    'permalink' => get_the_permalink(),
    		    'title' => get_the_title(),
    		    'banner' => get_field('banner'),
    		    'tile' => get_field('tile')
    		);

            $promotions[] = $promotion;

            $s++;

        endwhile; endif;
        
        wp_reset_query();

        return ($ppp > 1) ? $promotions : $promotions[0];

    }
