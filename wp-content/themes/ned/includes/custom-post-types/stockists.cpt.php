<?php

    // define cpt
    $cpt = array(
        'post_type_name' => 'stockist',
        'singular' => 'Stockist',
        'plural' => 'Stockists',
        'slug' => 'stockists'
    );

    // define cpt options
    $options = array(
        'supports' => array('title', 'editor', 'page-attributes'),
        'rewrite' => array(
            'slug' => 'stockist',
            'pages' => false
        ),
    );

    // create cpt
    $stockists_cpt = new CPT($cpt, $options);

    // set dashicon
    $stockists_cpt->menu_icon('dashicons-store');

    // add state taxonomy
    $stockists_cpt->register_taxonomy(array(
        'taxonomy_name' => 'stockist_state',
        'singular' => 'State',
        'plural' => 'States',
        'slug' => 'where-to-buy/state'
    ));

    // get stockist states
	function get_stockist_states($state=false) {

		$categories = array();
        $stockist_state_query = array();

        if($state) :
            $stockist_state_query['slug'] = $state;
        endif;

		$get_categories = get_terms(array('stockist_state'), $stockist_state_query);

		foreach($get_categories as $cat) :
			$categories[$cat->slug] = $cat->name;
		endforeach;

        //print_r($categories); exit;

		return $categories;

	}

    // function to retrieve stockists
    function get_stockists($state=false) {

        $stockists = array();

        // define query params
        $stockists_query = array(
            'post_status' => 'publish',
            'post_type' => 'stockist',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        );

		// get by category
		if($state) :

			$stockists_query['tax_query'] = array(
				array(
					'taxonomy' => 'stockist_state',
					'field'    => 'slug',
					'terms'    => $state,
				),
			);

		endif;

        //print_r($stockists_query); exit;

        // query db
        $get_stockists = new WP_Query($stockists_query);

        // get result
        $s = 1;
        if ( $get_stockists->have_posts() ) : while ( $get_stockists->have_posts() ) : $get_stockists->the_post();

            $stockist_id = get_the_ID();

            $state_term = get_field('state');
            $state = ($state_term) ? $state_term->name : '';

    		$stockist = array(
    		    'id' => $stockist_id,
    		    'name' => get_the_title(),
    		    'street_address' => get_field('street_address'),
    		    'suburb' => get_field('suburb'),
    		    'state' => $state,
    		    'postcode' => get_field('postcode'),
    		    'phone' => get_field('phone'),
    		    'opening_hours' => get_field('opening_hours'),
    		    'website' => get_field('website'),
    		    'email' => get_field('email'),
    		    'lat' => get_field('lat'),
    		    'lon' => get_field('lon'),
    		    'map_link' => get_field('map_link'),
    		);

            $stockists[] = $stockist;

            $s++;

        endwhile; endif;
        
        wp_reset_query();

        //print_r($stockists); exit;

        return $stockists;

    }


    function get_stockists_json($state=false) {
        $stockists = get_stockists($state);
        return json_encode($stockists);        
    }

