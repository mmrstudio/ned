<?php

    // define cpt
    $cpt = array(
        'post_type_name' => 'product',
        'singular' => 'Product',
        'plural' => 'Products',
        'slug' => 'products'
    );


    // create cpt
    $products_cpt = new CPT($cpt, $options);

    // set dashicon
    $products_cpt->menu_icon('dashicons-cart');
