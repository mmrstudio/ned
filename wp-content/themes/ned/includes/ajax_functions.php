<?php

    function add_ajax_action($action, $callback) {
        add_action('wp_ajax_'.$action, $callback);
        add_action('wp_ajax_nopriv_'.$action, $callback);
    }
    
    function request_param($key, $default) {
        return (isset($_REQUEST[$key])) ? $_REQUEST[$key] : $default ;
    }

    function ajax_check_age() {

        $dob_dd = intval(request_param('dob_dd', 0));
        $dob_mm = intval(request_param('dob_mm', 0));
        $dob_yyyy = intval(request_param('dob_yyyy', 0));
        $remember_me = request_param('remember_me', false);
    
        // check valid date
        if( ! checkdate($dob_mm, $dob_dd, $dob_yyyy) ) :
            echo 'invalid'; exit;
        endif;

        // check DOB
        $dob = new DateTime($dob_yyyy . '-' . $dob_mm . '-' . $dob_dd);
        $now = new DateTime();
        $years = $dob->diff($now);

        if( $years->y >= 18 ) :
            set_age_approved($remember_me);
            echo 'ok'; exit;
        else :
            echo 'underage'; exit;
        endif;

    }
    
    add_ajax_action('check_age', 'ajax_check_age');
