<?php

	require_once (TEMPLATEPATH . '/includes/instagram.class.php');
	$insta_access_token = '47133361.057d2ca.20d89e0b88b9445186bf7bf29a39fa5e';

    $instagram_client_id = get_field('instagram_client_id', 'option');
    $instagram_client_secret = get_field('instagram_client_secret', 'option');
    $instagram_access_token = get_field('instagram_access_token', 'option');

    if($instagram_client_id && $instagram_client_secret) :

        $instagram = new Instagram(array(
    		'apiKey'      => $instagram_client_id,
    		'apiSecret'   => $instagram_client_secret,
    		'apiCallback' => site_url(),
    	));

        if( isset($_GET['code']) ) :

            $instagram_credentials = $instagram->getOAuthToken($_GET['code']);

    		//print_r($instagram_credentials); exit;

            update_field('instagram_access_token', $instagram_credentials->access_token, 'option');

            wp_redirect(site_url('wp-admin/admin.php?page=options'));
            exit;

    	endif;

        if($instagram_access_token) :
            $instagram->setAccessToken($instagram_access_token);
        endif;

    else :

        $instagram = false;

    endif;

    //$json = '{"key":"testing \ud83d\ude04 my \ud83d\udc0e Emoji \ud83c\udf85 hureee \u260e hai \ud83d\udeb2"}';
    //$obj = json_decode($json);
    //print_r($obj); exit;

    //print_r(get_instagram_feed()); exit;

    // fetch the feed
    //get_instagram_feed(10);

	function time2str($ts) {
	   if(!ctype_digit($ts))
		   $ts = strtotime($ts);

	   $diff = time() - $ts;
	   if($diff == 0)
		   return 'now';
	   elseif($diff > 0)
	   {
		   $day_diff = floor($diff / 86400);
		   if($day_diff == 0)
		   {
			   if($diff < 60) return 'just now';
			   if($diff < 120) return '1 minute ago';
			   if($diff < 3600) return floor($diff / 60) . ' minutes ago';
			   if($diff < 7200) return '1 hour ago';
			   if($diff < 86400) return floor($diff / 3600) . ' hours ago';
		   }
		   if($day_diff == 1) return 'Yesterday';
		   if($day_diff < 7) return $day_diff . ' day' . ($day_diff == 1 ? '' : 's') . ' ago';
		   if($day_diff < 31) return ceil($day_diff / 7) . ' week' . (ceil($day_diff / 7) == 1 ? '' : 's') . ' ago';
		   if($day_diff < 60) return 'last month';
		   return date('F Y', $ts);
	   }
	   else
	   {
		   $diff = abs($diff);
		   $day_diff = floor($diff / 86400);
		   if($day_diff == 0)
		   {
			   if($diff < 120) return 'in a minute';
			   if($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
			   if($diff < 7200) return 'in an hour';
			   if($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
		   }
		   if($day_diff == 1) return 'Tomorrow';
		   if($day_diff < 4) return date('l', $ts);
		   if($day_diff < 7 + (7 - date('w'))) return 'next week';
		   if(ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' week' . (ceil($day_diff / 7) == 1 ? '' : 's');
		   if(date('n', $ts) == date('n') + 1) return 'next month';
		   return date('F Y', $ts);
	   }
	}

	function get_instagram_feed($limit=10, $delete_transient=false) {

		global $instagram;

		$instagram_images = array();

        if(! $instagram) return $instagram_images;

		if($delete_transient) delete_transient( 'instagram_images' );

		if ( false === ( $instagram_images = get_transient( 'instagram_images' ) ) ) :

			$instagram_response = $instagram->getUserMedia('self' , $limit);

            //print_r($instagram_response); exit;

			if( isset( $instagram_response->data ) ) :

                foreach( $instagram_response->data as $image_data ) :

					//$image_data->caption->text = removeEmoji($image_data->caption->text);

					$instagram_images[] = array(
                        'platform' => 'instagram',
                        'id' => $image_data->id,
                        'link' => $image_data->link,
                        'type' => $image_data->type,
                        'timestamp' => date('c', $image_data->created_time),
						'date' => date(get_option('date_format'), $image_data->created_time),
                        'date_hr' => time2str($image_data->created_time),
						'text' => $image_data->caption->text,
                        'image' => $image_data->images->standard_resolution->url,
                        'image_aspect' => ($image_data->images->standard_resolution->height / $image_data->images->standard_resolution->width) * 100,
                        'user_name' => $image_data->user->username,
                        'user_image' => $image_data->user->profile_picture,
                        'likes' => $image_data->likes->count,
                        'comments' => $image_data->comments->count,
					);

				endforeach;

			endif;

            $instagram_images = json_encode( $instagram_images );

			set_transient( 'instagram_images' , $instagram_images , 2 * HOUR_IN_SECONDS );

		endif;

        $instagram_images = json_decode( $instagram_images );

		//print_r($instagram_images); exit;

		return $instagram_images;

	}

    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array (
        	'key' => 'group_instagram_settings',
        	'title' => 'Instagram API',
        	'fields' => array (
        		array (
        			'key' => 'field_instagram_client_id',
        			'label' => 'Client ID',
        			'name' => 'instagram_client_id',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        		array (
        			'key' => 'field_instagram_client_secret',
        			'label' => 'Client Secret',
        			'name' => 'instagram_client_secret',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        		array (
        			'key' => 'field_instagram_access_token',
        			'label' => 'Access Token',
        			'name' => 'instagram_access_token',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        	),
        	'location' => array (
        		array (
        			array (
        				'param' => 'options_page',
        				'operator' => '==',
        				'value' => 'options',
        			),
        		),
        	),
        	'menu_order' => 1,
        	'position' => 'normal',
        	'style' => 'default',
        	'label_placement' => 'top',
        	'instruction_placement' => 'label',
        	'hide_on_screen' => '',
        	'active' => 1,
        	'description' => '',
        ));

    endif;

    function instagram_access_token_field($field) {

        global $instagram;

        if($field['key'] == 'field_instagram_access_token' && $instagram) :
            echo '<a href="' . $instagram->getLoginUrl() . '" accesskey="p" class="button button-primary button-large" style="margin-top: 10px;">Get Access Token</a>';
        endif;

    }

    add_action( 'acf/render_field/type=text', 'instagram_access_token_field', 10, 1 );
