<?php

    $header_page_title = 'News';

    // load header
    get_header();

    global $wp_query;

?>

            <div class="container-fluid">

                <div class="container">

                    <div class="row">

                        <div class="col-xs-12 news-articles archive">

<?php if ( have_posts() ) while ( have_posts() ) : the_post();  $post_date = explode(',', get_the_date('M,j,Y')); // start loop ?>

                            <div class="row news-article">

                                <div class="col-xs-12 article-body">

                                    <div class="article-date">
                                        <span class="month"><?php echo $post_date[0]; ?></span><br>
                                        <span class="day"><?php echo $post_date[1]; ?></span><br>
                                        <span class="year"><?php echo $post_date[2]; ?></span>
                                    </div>

                                    <div class="row">

                                        <div class="col-xs-12 col-sm-2 article-thumbnail hidden-xs">
                                            <?php if(has_post_thumbnail()) : the_post_thumbnail('thumbnail'); endif; ?>
                                        </div>

                                        <div class="col-xs-12 col-sm-2 article-thumbnail hidden-sm hidden-md hidden-lg">
                                            <?php if(has_post_thumbnail()) : the_post_thumbnail('large'); endif; ?>
                                        </div>


                                        <div class="col-xs-12 col-sm-10 col-md-7 col-lg-8">
                                            <h3 class="article-title"><?php the_title(); ?></h3>
                                            <div class="article-summary"><?php echo get_the_excerpt(); ?></div>
                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 read-more-button">
                                            <a href="<?php the_permalink(); ?>" class="btn btn-yellow btn-sml col-xs-12 pull-right read-more">Read More</a>
                                        </div>

                                    </div>

                                </div>

                            </div>

<?php endwhile; // end the loop ?>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12 news-pagination-wrap">
                            <div class="news-pagination">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-6 col-xs-offset-2 col-sm-offset-3 paginate-links">
                                        <div class="previous-page"><?php previous_posts_link('Previous'); ?></div>
                                        <div class="pages"><?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?> of <?php echo $wp_query->max_num_pages; ?></div>
                                        <div class="next-page"><?php next_posts_link('Next'); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

<?php

    get_footer();
