
                <div class="news-article-summary horizontal">

                    <h3 class="title"><?php the_title(); ?></h3>

                    <div class="col-xs-12 col-sm-6">

                        <div class="thumbnail">
                            <a href="<?php the_permalink(); ?>">

                                <div class="thumbnail">
                                    <?php $promotion_banner = get_field('banner'); ?>
                                    <img src="<?php echo $promotion_banner['sizes']['news-banner-small']; ?>">
                                </div>
    
                            </a>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6">

                        <div class="excerpt">
                            <p><?php the_excerpt(); ?></p>
                        </div>
    
                        <div class="read-more">
                            <a href="<?php the_permalink(); ?>" class="btn btn-orange btn-arrow btn-arrow-right">Read More</a>
                        </div>

                    </div>

                </div>
