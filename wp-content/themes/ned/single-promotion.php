<?php

    $header_page_title = 'News';

    // load header
    get_header();

    // get latest news articles
    $news_query = array(
        'post_status' => 'publish',
        'post_type' => 'post',
        'posts_per_page' => 3
    );

    $get_news = new WP_Query($news_query);

    global $wp_query;

?>

    <div class="container-fluid page-banner">

        <div class="container">
            <h1>Promotions</h1>
        </div>

    </div>

            <div class="container-fluid">

                <div class="container">

                    <div class="row">

                        <article class="col-xs-12 col-md-8 content-col">

<?php if ( have_posts() ) while ( have_posts() ) : the_post();  $post_date = explode(',', get_the_date('M,j,Y')); // start loop ?>

                            <div class="news-article">

                                <h2 class="title"><?php the_title(); ?></h2>

                                <div class="thumbnail">
                                    <?php
                                        $promotion_banner = get_field('banner');
                                        $promotion_tile = get_field('tile');
                                    ?>
                                    <img src="<?php echo $promotion_banner['sizes']['news-banner-large']; ?>">
                                </div>
            
                                <div class="article">
                                    <p><?php the_content(); ?></p>
                                </div>

								<div class="row">

									<div class="col-xs-12 col-sm-6 post-bottom-col">

		                                <div class="back-to-news">
		                                    <a href="<?php echo site_url('/promotions'); ?>" class="btn btn-black btn-arrow btn-arrow-left">Back to Promotions</a>
		                                </div>

									</div>

									<div class="col-xs-12 col-sm-6 post-bottom-col">

										<div class="share-post">
											<ul class="social-icons">
											    <li><a href="https://www.facebook.com/dialog/feed?app_id=419677871533284&link=<?php echo urlencode(get_the_permalink()); ?>&picture=<?php echo urlencode($promotion_tile['url']); ?>&name=<?php echo urlencode(str_replace('&#8211;', '-', get_the_title())); ?>&description=<?php echo urlencode(get_the_excerpt()); ?>&redirect_uri=http://drinkned.com.au/" target="_blank" class="linked-in icon icon-facebook">Facebook</a></li>
											    <li><a href="https://twitter.com/intent/tweet?text=<?php echo urlencode('@drinkned ' . str_replace('&#8211;', '-', get_the_title()) . ' - ' . get_the_permalink()); ?>" target="_blank" class="twitter icon icon-twitter">Twitter</a></li>
											    <?php /* <li><a href="#" target="_blank" class="twitter icon icon-email">Email</a></li> */ ?>
											</ul>
				                            <span>Share:</span>
				                        </div>

									</div>

								</div>

                            </div>

<?php endwhile; // end the loop ?>

                            <div class="other-news">

	                            <h2>Other Ned-Worthy News</h2>

								<?php get_template_part('news', 'latest-posts'); ?>

                            </div>

                        </article>

                        <aside class="col-xs-4 sidebar hidden-xs hidden-sm" id="sidebar">
                            <?php get_sidebar(); ?>
                        </aside>

                    </div>

                </div>

            </div>

<?php

    get_footer();
