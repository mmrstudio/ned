<?php

    // Check Age
    $age_approved = age_approved();
    if($age_approved === false) :
        wp_redirect('/age-gate');
    endif;
    
?><!DOCTYPE HTML>

<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title><?php echo get_page_title(); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<?php wp_head(); ?>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,600,700,800' rel='stylesheet' type='text/css'>

        <?php echo google_analytics('UA-58883042-1', 'auto'); ?>

    </head>

    <body <?php body_class(); ?>>

        <div class="responsive-nav-wrap">
            <nav class="responsive-nav">
                <?php output_main_nav(2); ?>
				<?php get_template_part('content', 'social-icons'); ?>
            </nav>
            <div class="responsive-nav-dismiss" id="responsiveNavDissmiss"></div>
        </div>

        <div class="container-fluid page-wrap">

            <header class="container header">

                <div class="row">

                    <div class="col-xs-12 col-sm-3">

                        <div class="ned-logo">
                            <a href="/" class="icon icon-ned-logo">Drink NED</a>
                        </div>

                    </div>

                    <div class="col-xs-9">

                        <div class="header-social hidden-xs">

                            <?php get_template_part('content', 'social-icons'); ?>

                            <span>Join the Gang:</span>

                        </div>

                        <nav class="main-nav hidden-xs">
                            <?php output_main_nav(1); ?>
                        </nav>

                    </div>

                </div>

                <button class="responsive-nav-toggle" id="responsiveNavToggle">Menu</button>

            </header>
