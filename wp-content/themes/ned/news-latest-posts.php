
    <?php

	    $get_latest_posts = new WP_Query(array(
	        'post_status' => 'publish',
	        'post_type' => 'post',
	        'posts_per_page' => 1,
	        'orderby' => 'date',
	        'order' => 'DESC'
	    ));

    ?>

    <div class="row">

        <?php if ( $get_latest_posts->have_posts() ) : while ( $get_latest_posts->have_posts() ) : $get_latest_posts->the_post(); ?>
        <div class="col-xs-12 col-sm-4">
            <?php get_template_part('news', 'summary'); ?>
        </div>
        <?php endwhile; endif; wp_reset_query(); ?>

    </div>

    <div class="row">

        <?php if ( $get_latest_posts->have_posts() ) : while ( $get_latest_posts->have_posts() ) : $get_latest_posts->the_post(); ?>
        <div class="col-sm-4 hidden-xs">

            <div class="read-more">
                <a href="<?php the_permalink(); ?>" class="btn btn-orange btn-arrow btn-arrow-right">Read More</a>
            </div>

        </div>
        <?php endwhile; endif; wp_reset_query(); ?>

    </div>
