<?php

    /* Template Name: Page - News */

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

         // load header
         get_header();

?>

    <div class="container-fluid page-banner">

        <div class="container">
            <h2>News</h2>
        </div>

    </div>

            <div class="container-fluid">

                <div class="container">

                    <div class="row">

                        <article class="col-xs-12 col-md-9 content-col">

                            <div class="row">
                    
                                <?php
                    
                                $get_latest_posts = new WP_Query(array(
                                    'post_status' => 'publish',
                                    'post_type' => 'post',
                                    'posts_per_page' => 1
                                ));
                    
                                ?>
                    
                                <?php if ( $get_latest_posts->have_posts() ) : while ( $get_latest_posts->have_posts() ) : $get_latest_posts->the_post(); ?>
                                 <div class="news-article-summary">
                                 
                                 
									<div class="row">
										<div class="title col-md-11 col-md-push-1">
											<h1><?php the_title(); ?></h1>
											<span ><?php echo get_the_date('j', get_the_ID() ); ?> <?php echo get_the_date('M', get_the_ID() ); ?> <?php echo get_the_date('Y', get_the_ID() ); ?></span>
										</div>
									</div>
									<div class="row">
										<div class="thumbnail">



											<?php the_post_thumbnail('news-banner-large'); ?>

										</div>

									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-11 post-bottom-col col-sm-push-1">
											<div class="article">
												<?php the_content(); ?>
											</div>
										</div>
										<div class="col-xs-12 col-sm-1 post-bottom-col col-xs-12 col-sm-pull-11">

											<div class="share-post">
												<span>Share</span>
												<ul class="social-icons">
													<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_id()), 'news-banner-large'); ?>
													<li><a href="https://www.facebook.com/dialog/feed?app_id=419677871533284&link=<?php echo urlencode(get_the_permalink()); ?>&picture=<?php echo urlencode($thumb[0]); ?>&name=<?php echo urlencode(str_replace('&#8211;', '-', get_the_title())); ?>&description=<?php echo urlencode(get_the_excerpt()); ?>&redirect_uri=http://drinkned.com.au/" target="_blank" class="linked-in icon icon-facebook_share">Facebook</a></li>
													<li><a href="https://twitter.com/intent/tweet?text=<?php echo urlencode('@drinkned ' . str_replace('&#8211;', '-', get_the_title()) . ' - ' . get_the_permalink()); ?>" target="_blank" class="twitter icon icon-twitter_share">Twitter</a></li>
													<li><a href="mailto:?subject=I wanted you to see this NED news&amp;body=Check out this news <?php echo(get_the_permalink()); ?>" target="_blank" class="twitter icon icon-email_share icon-email">Email</a></li> 
												</ul>

											</div>

										</div>

										
									</div>
								</div>

                                <?php endwhile; endif; wp_reset_query(); ?>
                    
                            </div>

                        </article>

                        <aside class="col-xs-12 col-sm-3 newssidebar " >
							<h5>RECENT NEWS</h5>
                       
                       		<?php
                    
                                $get_latest_posts = new WP_Query(array(
                                    'post_status' => 'publish',
                                    'post_type' => 'post',
									'posts_per_page' => 6,
									"orderby" => "modified",
      								"order" => 'DESC',
                                    
                                ));
                    
                                ?>
                    
                                <?php if ( $get_latest_posts->have_posts() ) : while ( $get_latest_posts->have_posts() ) : $get_latest_posts->the_post(); ?>
                                 <div class="news-sidebar">
                                 
                                 
									<div class="row">
										<div class="title col-md-12 ">
											<h6><a href=" <?php the_permalink(); ?>"><?php the_title(); ?>  <span ><?php echo get_the_date('j', get_the_ID() ); ?> <?php echo get_the_date('M', get_the_ID() ); ?></span></a></h6>
											
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12">
											<div class="article">
												<a href=" <?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
											</div>
										</div>
									</div>
								</div>

                                <?php endwhile; endif; wp_reset_query(); ?>
                        </aside>

                    </div>

                </div>

            </div>

<?php

        get_footer();

    endwhile; // end the loop
