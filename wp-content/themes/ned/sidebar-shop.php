
    <?php global $woocommerce; ?>

    <div class="sidebar-cart-contents">
        <h3>Cart</h3>
        <strong><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></strong><br>
        <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">View Cart</a>
    </div>

    <nav class="shop-categories">
        <ul>
            <?php wp_list_categories(array('taxonomy' => 'product_cat', 'title_li' => false));  ?>
        </ul>
    </nav>

    <?php //woocommerce_catalog_ordering(); ?>

    <div class="sidebar-promotion">

        <?php $latest_promotion = get_promotions(1); ?>

        <a href="<?php echo $latest_promotion['permalink']; ?>">
            <img src="<?php echo $latest_promotion['tile']['sizes']['news-banner-tile']; ?>">
        </a>

    </div>

    <?php //get_template_part('content', 'where-to-buy'); ?>
