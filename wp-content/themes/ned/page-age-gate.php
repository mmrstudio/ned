<?php

    /* Template Name: Age Gate */

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

?>

<!DOCTYPE HTML>

<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title><?php echo get_page_title(); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<?php wp_head(); ?>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,600,700,800' rel='stylesheet' type='text/css'>

        <script> var wp_ajax_url = '<?php echo site_url('/wp-admin/admin-ajax.php'); ?>'; </script>

        <?php echo google_analytics('UA-58883042-1', 'auto'); ?>

    </head>

    <body <?php body_class(); ?>>

        <div class="container-fluid page-wrap age-gate">

            <div class="container-fluid hero-banner">

                <div class="container">

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 col-md-5">
                            <h1>Please enter your date of birth to enter</h1>

                            <form action="<?php echo site_url('/age-gate'); ?>" method="post" class="age-gate-dob" id="ageGateForm">

                                <div class="form-group">
                                    <input type="number" name="dob_dd" id="dob_dd" class="dob_dd" placeholder="DD" min="1" max="31" data-maxlength="2" data-next=".dob_mm" data-validation="Please enter a valid day (01 to 31)">
                                </div>

                                <span class="slash">/</span>

                                <div class="form-group">
                                    <input type="number" name="dob_mm" id="dob_mm" class="dob_mm" placeholder="MM" min="1" max="12" data-maxlength="2" data-next=".dob_yyyy" data-prev=".dob_dd" data-validation="Please enter a valid month (01 to 12)">
                                </div>

                                <span class="slash">/</span>

                                <div class="form-group">
                                    <input type="number" name="dob_yyyy" id="dob_yyyy" class="dob_yyyy" placeholder="YYYY" min="1880" max="<?php echo date('Y')-10; ?>" data-maxlength="4" data-prev=".dob_mm">
                                </div>

                                <div class="form-group submit">
                                    <?php wp_nonce_field( 'ajax-age-gate-nonce', 'wp_nonce' ); ?>
                                    <button type="submit" class="btn btn-black btn-arrow btn-arrow-right btn-xlg">Enter</button>
                                </div>

                                <div class="remember-me">
                                    <label>
                                        <input type="checkbox" name="remember_me" id="remember_me" value="1" checked>
                                        Remember me next time I visit
                                    </label>
                                </div>

                            </form>

                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-7">

                            <div class="ned-can">
                                <img src="<?php echo theme_img('ned-cans-front.png'); ?>">
                            </div>

                        </div>

                    </div>
        
                </div>
        
            </div>

            <div class="container">
                <div class="col-xs-12 col-lg-5 remember-me-col">
                    <div class="over-18"></div>
                </div>
            </div>

        </div>

<?php wp_footer(); ?>

    </body>

</html>

<?php endwhile; // end loop ?>