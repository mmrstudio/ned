<?php

    // Load extra helper functions
    require_once (TEMPLATEPATH . '/includes/helper_functions.php');
    require_once (TEMPLATEPATH . '/includes/twitter.api.php');
    require_once (TEMPLATEPATH . '/includes/ajax_functions.php');
	require_once (TEMPLATEPATH . '/includes/facebook.api.php');
	require_once (TEMPLATEPATH . '/includes/instagram.api.php');
	
	

    // Load custom post types
    require_once (TEMPLATEPATH . '/includes/classes/CPT.php');
    require_once (TEMPLATEPATH . '/includes/custom-post-types/ned-tv.cpt.php');
    require_once (TEMPLATEPATH . '/includes/custom-post-types/promotions.cpt.php');
    require_once (TEMPLATEPATH . '/includes/custom-post-types/stockists.cpt.php');
	require_once (TEMPLATEPATH . '/includes/custom-post-types/products.cpt.php');
	require_once (TEMPLATEPATH . '/includes/custom-post-types/social.cpt.php');

    // Set default CONSTANTS
    define( 'THEME_URL' , get_template_directory_uri() );
    define( 'PAGE_BASENAME' , get_current_basename() );

    // Add theme supports
    add_theme_support('post-thumbnails');

    // Register navigation
    register_nav_menu( 'main-nav' , 'Main Menu' );

    // Add image sizes
    add_image_size('news-banner-large', 720 , 300 , TRUE );
    add_image_size('news-banner-small', 370 , 154 , TRUE );
    add_image_size('news-banner-tile', 370 , 300 , TRUE );

    // Set default image link type
    update_option('image_default_link_type','file');

    // Set JPG quality for image resizing
    add_filter( 'jpeg_quality', 'set_jpg_quality' );
    function set_jpg_quality( $quality ) { return 95; }

    // Load stylesheets and javascript files
    add_action('init', 'theme_init');

    // Initilization function
    function theme_init() {

        // Load stylesheets and javascript files
        load_scripts_and_styles();

        // Create options page
        create_theme_options_page();

        // hide stuff we don't need
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'feed_links');
		
		
	   // Create options page
	   if( function_exists('acf_add_options_page') ) {

		   $options = acf_add_options_page(array(
			   'page_title'     => 'Options',
			   'menu_title'     => 'Options',
			   'menu_slug'     => 'options',
			   'capability'     => 'edit_posts',
			   'redirect'     => false
		   ));

	   }


    }



	add_filter( 'gform_pre_submission_filter' , "foo_show_confirmation_and_form" , 10 , 1 );
	
	function foo_show_confirmation_and_form($form) {
		
		if($form['id'] != 2){
			return $form;
		}
		
		$shortcode = '[gravityform id="' . $form['id'] . '" title="false" description="false" ]';

		if ( array_key_exists( 'confirmations' , $form ) ) {
		foreach ( $form[ 'confirmations' ] as $key => $confirmation ) {
		  $form[ 'confirmations' ][ $key ][ 'message' ] .= $shortcode;
		}
		}

		if ( array_key_exists( 'confirmation' , $form ) ) {
		$form[ 'confirmation' ][ 'message' ] .= $shortcode;
		
		}
		//var_dump($form );
		return $form;
	}
	
	add_filter("gform_confirmation_anchor", create_function("","return true;"));





    function load_scripts_and_styles() {

        if( ! is_admin() ) :

            // Add stylesheets
            wp_enqueue_style( 'main_styles' , THEME_URL . '/style.css' , FALSE , '0.1.0' , 'screen' );

            // Load scripts
            add_action('wp_enqueue_scripts', 'load_scripts', 0);

            // Replace jQuery with Google CDN version
            function load_scripts() {

                // Load jQuery from CDN
                $jquery_version = '1.11.1';
                wp_deregister_script( 'jquery' );
                wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . $jquery_version . '/jquery.min.js' , array() , $jquery_version , FALSE );
                wp_enqueue_script( 'jquery' );

                // Load modernizr
                wp_enqueue_script( 'modernizr' , THEME_URL . '/js/vendor/modernizr.min.js' , '' , '0.1.0' , FALSE );

                // Load main scripts
               
                wp_enqueue_script( 'main_script' , THEME_URL . '/js/main.js' , 'jquery' , '0.1.0' , TRUE );
            }

        endif;

    }

    function create_theme_options_page() {

        if( function_exists('acf_add_options_page') ) {
         
        	$page = acf_add_options_page(array(
        		'page_title' 	=> 'Header &amp; Footer',
        		'menu_title' 	=> 'Header &amp; Footer',
        		'menu_slug' 	=> 'header-footer-settings',
        		'capability' 	=> 'edit_posts',
        		'redirect' 	=> false
        	));
         
        }

    }

    // Increase excerpt length
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
    function custom_excerpt_length( $length ) { return 25; }

    // Filter HTML in Wordpress text widget
    add_filter('widget_text', 'filter_text_widget');
    function filter_text_widget( $text ) { return apply_filters('the_content', $text); }

    // Remove hardcoded width/height attrs from post thumbnails
    add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
    function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
        return $html;
    }

    // Remove dimension attributes from img tags in content
    add_filter('the_content', 'remove_img_dimensions', 10);
    function remove_img_dimensions($html) {
        $html = preg_replace('/(width|height)=["\']\d*["\']\s?/', "", $html);
        return $html;
    }

    // Add nav menu item names to links
    add_filter( 'nav_menu_css_class', 'add_nav_item_name_class', 10, 2 );
    function add_nav_item_name_class( $classes , $item ) {
        $new_class = strtolower( preg_replace("/[^A-Za-z0-9 ]/", '-', $item->title) );
        $classes[] = $new_class;
        $classes[] = $new_class.'-link';
        return $classes;
    }

    // remove admin menu items
    add_action( 'admin_menu', 'remove_admin_links' );
    function remove_admin_links() {
        remove_menu_page('edit-comments.php');
    }


    // wp-admin login screen logo
/*
    add_action('login_enqueue_scripts', 'admin_login_logo');
    function admin_login_logo() {
        echo '<style> body { background: url('.THEME_URL.'/images/masthead-bg.jpg) no-repeat center bottom #041c2c !important; } .login #backtoblog a, .login #nav a { color: #fff !important; } .login h1 a { background: url('.THEME_URL.'/images/login-logo.png) no-repeat center center transparent; background-size: auto auto; width: 320px; } </style>'."\n";
    }
*/

/*
    add_filter( 'nav_menu_css_class', 'remove_uneccessary_nav_class', 10, 2 );
    function remove_uneccessary_nav_class( $classes , $item ){
        $post_type = get_post_type();
        if ($post_type != 'post' && $item->object_id == get_option('page_for_posts')) :
            $current_value = "current_page_parent"; 
            $classes = array_filter($classes, function ($element) use ($current_value) { return ($element != $current_value); } );
        endif;
    	return $classes;
    }
*/

    function add_slug_body_class( $classes ) {
        global $post;
        if ( isset( $post ) ) { $classes[] = $post->post_type . '-' . $post->post_name; }
        return $classes;
    }
    add_filter( 'body_class', 'add_slug_body_class' );

    define('FIRST_VISIT', is_first_time());

    function get_tabbed_content($parent_id=false, $post_type='page') {

        $tabbed_content = false;
        $tabs = array();

        $get_tabbed_content = new WP_Query(array(
            'post_type' => $post_type,
            'post_parent' => $parent_id,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC',
        ));

        if($get_tabbed_content->have_posts()) :

            $tabbed_content = $get_tabbed_content;

            while($get_tabbed_content->have_posts()) :
                $get_tabbed_content->the_post();
                global $post;

                $tabs[] = array(
                    'title' => get_the_title(),
                    'id' => $post->post_name,
                );

            endwhile;

            wp_reset_query();

            return array(
                'tabs' => $tabs,
                'content' => $get_tabbed_content,
            );

        endif;

        return false;

    }

    function get_page_title() {

        if( is_front_page() ) :
            return get_bloginfo('name');
        else :
            return wp_title( '|', false, 'right' ) . get_bloginfo('name');
        endif;

    }

    // gravity forms filters
	 
	
	add_action( 'gform_after_submission_2', 'after_submission', 10, 2 );

	function after_submission($button, $form){
        return "<button class='btn btn-lg btn-white' id='gform_submit_button_{$form["id"]}'><span>haha</span></button>";
    }	

    add_filter("gform_submit_button_2", "newsletter_submit_button", 10, 2);
    function newsletter_submit_button($button, $form){
        return "<button class='btn btn-lg btn-white' id='gform_submit_button_{$form["id"]}'><span>JOIN</span></button>";
    }

	add_filter("gform_submit_button_1", "enquiry_submit_button", 10, 2);
    function enquiry_submit_button($button, $form){
        return "<button class='btn btn-lg btn-white' id='gform_submit_button_{$form["id"]}'><span>Submit</span></button>";
    }

    add_filter("gform_validation_message_2", "change_message", 10, 2);
    function change_message($message, $form) {
        $popover = "\n".'<script> jQuery(document).ready(function($) { window.gform =  $("#gform_'.$form['id'].'"); console.log("gform"); gform.popover({ trigger : "manual", content: "Please enter both your name and email address below...", placement: "top" }); gform.popover("show"); }); </script>';
        return $popover;
    }

    function theme_img($image) {
        return get_template_directory_uri() . '/images/' . $image;
    }

    // SHOP ACTIONS
    add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );

    // send order notifications to shop@drinkned.com.au
    add_filter( 'woocommerce_email_recipient_new_order', 'wc_new_order_shop_at_drinkned', 10, 2 );
    function wc_new_order_shop_at_drinkned( $recipient, $order ) {
        return 'shop@drinkned.com.au, ' . $recipient;
    }
	
	
	
	
	
	
add_action( 'wp_ajax_nopriv_load_result', 'load_result' );


add_action( 'wp_ajax_load_result', 'load_result' );



function load_result() {
	
	
	wp_reset_query();
	
	
	$request_title = $_POST['title'];
	
	$post = get_page_by_title($request_title, OBJECT, 'post');

		
	$args = array(
		'post_type'      => 'season',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
		'p' => $post ->ID
		);

		$posts = new WP_Query($args);
	
	
	
	query_posts($args);
	
		
		@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

	
	?>
	<select class="season_filter">
    <?php if($posts->have_posts()):  ?>
    
    	
		<?php while ( $posts->have_posts() ) : $posts->the_post(); 
		
			$date = explode("/", get_field('date'));
			$year = $date[2];
        	if ($year == $request_year){
				echo '<option>'.get_the_title().'</option>';	
			}
            
        endwhile; ?>
    <?php endif; ?>
	</select>
    
    
	<?php die();
} 

/**
 * @snippet       Hide Products From Specific Category @ Shop
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=572
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.1.2
 */
 
add_action( 'woocommerce_product_query', 'bbloomer_hide_products_category_shop' );
  
function bbloomer_hide_products_category_shop( $q ) {
 
    $tax_query = (array) $q->get( 'tax_query' );
 
    $tax_query[] = array(
           'taxonomy' => 'product_cat',
           'field' => 'slug',
           'terms' => array( 'home' ), // Category slug here
           'operator' => 'NOT IN'
    );
 
 
    $q->set( 'tax_query', $tax_query );
 
}
?>