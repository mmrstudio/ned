<?php
define('WP_AUTO_UPDATE_CORE', false);// This setting was defined by WordPress Toolkit to prevent WordPress auto-updates. Do not change it to avoid conflicts with the WordPress Toolkit auto-updates feature.
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ned_database'); //ned_database

/** MySQL database username */
define('DB_USER', 'root'); //ned_database

/** MySQL database password */
define('DB_PASSWORD', 'root');
 //qb6u8emn419
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DISALLOW_FILE_EDIT', TRUE); // Sucuri Security: Wed, 08 Feb 2017 03:09:02 +0000


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g$Lz5,`8)A8:}~y6c@S30X[=AN+><ZS-Y!2cg.=iaY-/.7$DQxtP*@+H.qD+wAe$');
define('SECURE_AUTH_KEY',  '-0=q)+j42z@osX^+nB%H9:tUzJhG?SG$]|u%gRZ9_)r%udQVe9/@Z^@r48RD-HhM');
define('LOGGED_IN_KEY',    '@Kz~Ab0Re`<;$7I(?&8ytm3M-`<=+mi|++cFf65j^z{z.|sw#|#>.CqVu:pwEl!A');
define('NONCE_KEY',        '2-;O]l3C>{DRp!%C k-(m+#%/Tx%cofKsp)^s2egZiHwj(iibG^SC#-no?gTkTk{');
define('AUTH_SALT',        'bC{z`vT6%X@xe,V@7?]!{.WgV+xZDQx0Qfd7/R8u|i1b@.%I<*|Ws%|l|-Y)<|C]');
define('SECURE_AUTH_SALT', ',++]Hu|(ABL{;#gisr[W{~^=G;k5Wk$d|QgX#enbu7=JT3i!vgXq<a2?8]c2zt)?');
define('LOGGED_IN_SALT',   '>+!;dj1#hB$BY)2RcGgs-BVodS `0H!r6zn{yb!*TeK](|rin/?%=@O;~eMSn-6<');
define('NONCE_SALT',       'WWrhoX/UEM|HH.o0` $$wdzP9)b>cX{4@NW{7N[vL5]^jBRv)h=;Dd$xSt*,LK{L');/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');
